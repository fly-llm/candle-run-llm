#!/bin/sh

# 安装软件下载模型
pip3 install huggingface_hub

# 永久替换：https://developer.aliyun.com/mirror/rustup
echo 'export RUSTUP_DIST_SERVER=https://rsproxy.cn' >> ~/.bashrc
echo 'export RUSTUP_UPDATE_ROOT=https://rsproxy.cn/rustup' >> ~/.bashrc
source ~/.bashrc

# 临时替换
export RUSTUP_DIST_SERVER="https://rsproxy.cn"
export RUSTUP_UPDATE_ROOT="https://rsproxy.cn/rustup"

export RUSTUP_HOME=/root/autodl-tmp/cargo
export CARGO_HOME=/root/autodl-tmp/cargo

# 安装rust 软件
# 选择默认安装模式 官方脚本：
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

# https://rsproxy.cn/
curl --proto '=https' --tlsv1.2 -sSf https://rsproxy.cn/rustup-init.sh | sh -s -- -y


rustup default stable

rustc -V