import os
import sys

os.environ['HF_ENDPOINT'] = 'https://hf-mirror.com'

# 设置hf 路径：
#export HF_HOME=/root/autodl-tmp/hf_cache

os.environ['HF_HOME'] = '/root/autodl-tmp/hf_cache'

if len(sys.argv) <= 1:
    print("参数错误！请执行：python3 download.py Qwen/Qwen1.5-0.5B")
    sys.exit(0)

print('argv: ', str(sys.argv))
repo_id=sys.argv[1]
from huggingface_hub import snapshot_download
#snapshot_download(repo_id="Qwen/Qwen1.5-0.5B")
# https://hf-mirror.com/Qwen/Qwen1.5-0.5B
snapshot_download(repo_id=repo_id)
