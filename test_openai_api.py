# encoding:utf-8

import openai
import json

client = openai.OpenAI(
    base_url="http://127.0.0.1:9997/v1",
)
messages = [
    {"role": "system", "content": "你是一个有用的助手。不要对要函数调用的值做出假设。"},
    {"role": "user", "content": "北京 现在的天气怎么样？"}
]

tools = [
    {
        "type": "function",
        "function": {
            "name": "get_current_weather",
            "description": "获取当前天气",
            "parameters": {
                "type": "object",
                "properties": {
                    "location": {
                        "type": "string",
                        "description": "城市，例如北京",
                    },
                    "format": {
                        "type": "string",
                        "enum": ["celsius", "fahrenheit"],
                        "description": "使用的温度单位。从所在的城市进行推断。",
                    },
                },
                "required": ["location", "format"],
            },
        },
    }
]

chat_completion = client.chat.completions.create(
    model="chatglm3",
    messages=messages,
    tools=tools,
    temperature=0.7
)
func_name = chat_completion.choices[0].message.tool_calls[0].function.name
print('func_name', func_name)
func_args = chat_completion.choices[0].message.tool_calls[0].function.arguments
func_args_dict = json.loads(func_args)
print('func_args', func_args_dict['location'])