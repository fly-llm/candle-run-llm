import openai

client = openai.Client(
    api_key="cannot be empty",
    base_url=f"http://127.0.0.1:9997/v1"
)
response = client.chat.completions.create(
    model="qwen-vl-chat", # qwen-vl-chat 或者 yi-vl-chat
    messages=[
        {
            "role": "user",
            "content": [
                {"type": "text", "text": "这个图片是什么？"},
                {
                    "type": "image_url",
                    "image_url": {
                        "url": "https://p3.dcarimg.com/img/motor-mis-img/2465f0c78280efddcc305991bd1f2ea2~2508x0.jpg",
                    },
                },
            ],
        }
    ],
)
print(response.choices[0])